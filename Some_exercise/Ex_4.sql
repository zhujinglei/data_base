--Runable
--Create all tables
CREATE DATABASE COMPANY
USE COMPANY
    CREATE TABLE EMPLOYEE(
        FNAM TEXT NOT NULL,
        LNAME TEXT NOT NULL,
        NI VARCHAR(9) NOT NULL,
        BDATE DATE,
        ADDRESS TEXT,
        GENDER TEXT,
        SALARY DECIMAL,
        SUPERNI VARCHAR(9),
        DNO INT UNSIGNED,
        PRIMARY KEY (NI),
        FOREIGN KEY (SUPERNI)
            REFERENCES EMPLOYEE(NI)
            ON DELETE CASCADE ON UPDATE CASCADE
    );
-- foreign key to project is not set

CREATE TABLE DEPARTMENT (
    DNAME TEXT,
    DNUM INT UNSIGNED,
    MGRINI VARCHAR(9),
    MGRSTARTDATE DATE,
    PRIMARY KEY (DNUM),

    FOREIGN KEY (MGRINI)
        REFERENCES EMPLOYEE(NI)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE DEPT_LOCATIONS (
    DNO INT UNSIGNED,
    DLOCATION VARCHAR(255),
    PRIMARY KEY (DNO, DLOCATION),

    FOREIGN KEY (DNO)
        REFERENCES DEPARTMENT(DNUM)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE PROJECT (
    PNAME TEXT,
    PNUM INT UNSIGNED,
    PLOCATION TEXT,
    DNO INT UNSIGNED,
    PRIMARY KEY (PNUM),
    
    FOREIGN KEY (DNO)
        REFERENCES DEPARTMENT(DNUM)
        ON DELETE CASCADE ON UPDATE CASCADE
);

ALTER TABLE EMPLOYEE 
ADD 
    CONSTRAINT 
    FOREIGN KEY (DNO)
        REFERENCES DEPARTMENT(DNUM) 
        ON DELETE CASCADE ON UPDATE CASCADE;
        
CREATE TABLE WORKS_ON (
    ENI VARCHAR(9),
    PNO INT UNSIGNED,
    HOURS INT UNSIGNED,
    PRIMARY KEY (ENI, PNO),
    
    FOREIGN KEY (ENI)
        REFERENCES EMPLOYEE(NI)
        ON DELETE CASCADE ON UPDATE CASCADE,

    FOREIGN KEY (PNO)
        REFERENCES PROJECT(PNUM)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE DEPENDENT (
    ENI VARCHAR(9),
    DEPENDENT_NAME VARCHAR(255),
    GENDER TEXT,
    BDATE DATE,
    RELATIONSHIP TEXT,
    PRIMARY KEY (ENI, DEPENDENT_NAME),
    
    FOREIGN KEY (ENI)
        REFERENCES EMPLOYEE(NI)
        ON DELETE CASCADE ON UPDATE CASCADE
)


-- Prepare the data
INSERT INTO `EMPLOYEE` (`FNAM`, `LNAME`, `NI`, `BDATE`, `ADDRESS`, `GENDER`, `SALARY`, `SUPERNI`, `DNO`)
VALUES
	('First Name 1','Last Name 1','NI1234567','1978-01-01','Not real address','None',1,'NI1234567',NULL),
	('First Name 2','Last Name 2','NI1234568','1978-01-01','Not real address','None',2,'NI1234567',NULL),
	('First Name 3','Last Name 3','NI1234569','1978-01-01','Not real address','None',3,'NI1234568',NULL),
	('First Name 4','Last Name 4','NI1234590','1978-01-01','Not real address','None',4,'NI1234567',NULL),
	('First Name 5','Last Name 5','NI1234591','1978-01-01','Not real address','None',5,'NI1234567',NULL),
	('First Name 6','Last Name 6','NI1234592','1978-01-01','Not real address','None',6,'NI1234590',NULL);

INSERT INTO `DEPARTMENT` (`DNAME`, `DNUM`, `MGRINI`, `MGRSTARTDATE`)
VALUES
	('Test_Department_1',1,'NI1234567','1999-01-01'),
	('Test_Department_2',2,'NI1234590','1999-01-01');

INSERT INTO `DEPENDENT` (`ENI`, `DEPENDENT_NAME`, `GENDER`, `BDATE`, `RELATIONSHIP`)
VALUES
	('NI1234567','Test partener 1','None','1999-01-01','None');

INSERT INTO `DEPT_LOCATIONS` (`DNO`, `DLOCATION`)
VALUES
	(1,'Nowhere-1'),
	(2,'Nowhere-2');

INSERT INTO `PROJECT` (`PNAME`, `PNUM`, `PLOCATION`, `DNO`)
VALUES
	('Test project not real 1',1,'Nowhere',1),
	('Test project not real 2',2,'Nowhere',1);

INSERT INTO `WORKS_ON` (`ENI`, `PNO`, `HOURS`)
VALUES
	('NI1234567',1,20),
	('NI1234568',1,20),
	('NI1234569',1,20),
	('NI1234590',2,1),
	('NI1234591',2,22);

UPDATE `EMPLOYEE`
    SET `DNO` = CASE `SALARY`
        WHEN 1 THEN 1
        WHEN 2 THEN 1
        WHEN 3 THEN 1
        WHEN 4 THEN 2
        WHEN 5 THEN 1
        WHEN 6 THEN 2
    END








EX 2

-- select distinct salary number
SELECT COUNT(DISTINCT SALARY) FROM EMPLOYEE
-- find high avg salary depart (4 instead of 42000)
SELECT D.DNAME, E.SALARY
FROM (SELECT 
        AVG(SALARY) AS SALARY, DNO
      FROM EMPLOYEE GROUP BY DNO
    ) E, DEPARTMENT D
WHERE
    E.DNO=D.DNUM AND E.SALARY>4
-- find out department info
SELECT 
    D.DNUM AS DEPARTMENT_NUM, 
    COUNT(E.NI) AS DEPARTMENT_EMPLOYEE_COUNT,
    AVG(E.SALARY) AS AVERAGE_SALARY
FROM
    DEPARTMENT D, EMPLOYEE E
WHERE 
    D.DNUM=E.DNO
GROUP BY
    D.DNUM
-- 
SELECT
	P.PNAME, P.PNUM, COUNT(DISTINCT E.NI) AS E_COUNT
FROM 
	PROJECT P, EMPLOYEE E, DEPARTMENT D
WHERE 
	E.DNO=D.DNUM AND P.DNO=D.DNUM
GROUP BY P.PNUM
HAVING E_COUNT>2
--
SELECT 
	SUM(E.SALARY), MAX(E.SALARY), MIN(E.SALARY), AVG(E.SALARY)
FROM
	EMPLOYEE E, DEPARTMENT D
WHERE 
	E.DNO=D.DNUM AND D.DNAME='TEST_DEPARTMENT_1'
--
SELECT 
	(SELECT COUNT(NI) FROM EMPLOYEE) AS TOTAL_EMPLOYEE, 
	COUNT(E.NI) AS D_1_EMPLOYEE
FROM
	EMPLOYEE E, DEPARTMENT D
WHERE 
	E.DNO=D.DNUM AND D.DNAME='TEST_DEPARTMENT_1'
--
SELECT
	DISTINCT(E.NI)
FROM
	EMPLOYEE E, PROJECT P, DEPARTMENT D
WHERE 
	E.DNO=D.DNUM AND (P.PNUM=1 OR P.PNUM=2) AND P.DNO=D.DNUM
--
SELECT 
	D.DNUM, COUNT(E.NI)
FROM 
	DEPARTMENT D, EMPLOYEE E
WHERE E.SALARY > 1 AND D.DNUM=E.DNO
GROUP BY D.DNUM
HAVING COUNT(E.NI)>1