

DROP DATABASE IF EXISTS company_lab;
CREATE DATABASE IF NOT EXISTS company_lab;
USE company_lab;

SELECT 'CREATING DATABASE STRUCTURE' as 'INFO';

DROP TABLE IF EXISTS employee,
                     department,
                     project,
                     works_on,
                     dep_location,
                     dependent;

/*!50503 set default_storage_engine = InnoDB */;
/*!50503 select CONCAT('storage engine: ', @@default_storage_engine) as INFO */;

CREATE TABLE employee (
    NI      INT             NOT NULL,
    bdate  DATE            NOT NULL,
    fname  VARCHAR(14)     NOT NULL,
    lname   VARCHAR(16)     NOT NULL,
    gender      ENUM ('M','F')  NOT NULL,    
    hire_date   DATE            NOT NULL,
     job char(15),
    salary decimal(14,2),
    dno INT NOT NULL,
    superNI INT             NOT NULL,
         PRIMARY KEY (NI)
);

INSERT INTO `employee` (`NI`, `bdate`, `fname`, `lname`, `gender`, `hire_date`, `salary`,`Dno`,`superNI`,`job`) VALUES
(1, '1985-10-11','rogers','sanders','M','2016-08-02', '20000.00', 2, 3, 'developer'),
(2, '1985-10-04', 'mike','miller','M', '2016-11-02',NULL , 4, 4, 'not assinged'),
(3, '1950-10-11','david','brown','M','1999-10-11', '40000.00',1, 9, 'team leader'),
(4, '1942-10-11','ali','morgan','M','1975-10-11','70000.00',2, 8, 'manager'),
(5, '1986-10-08','john','wright','M', '2016-08-02','20000.00',4, 3, 'developer'),
(6, '1980-10-11','alex','john','F','2010-10-11', '45000.00',3, 9, 'project manager'),
(7,'1981-05-12', 'james','andrews','M','2011-10-05','60000.00',3, 8, 'manager'),
(8, '1940-08-07','jenny','ross', 'F','1980-10-11', '80000.00',1, 8, 'executive'),
(9, '1950-12-01','paul','bell','M', '1990-10-11','50000.00',2, 4, 'data analyst');


CREATE TABLE department (
    dnum     INT        NOT NULL,
    dname   VARCHAR(40)     NOT NULL,
    mgrNI INT NOT NULL,
    mgrstartdate DATE,
    FOREIGN KEY (mgrNI) REFERENCES employee (NI) ON DELETE CASCADE,
    PRIMARY KEY (dnum),
    UNIQUE  KEY (dname)
);
INSERT INTO `department` (`dnum`, `dname`, `mgrNI`, `mgrstartdate`) VALUES
(1, 'research',8, '2000-10-11'),
(2, 'hr',7, '2017-10-05'),
(3, 'technology',9, '2001-10-11'),
(4, 'payroll',4, '2000-03-11');

CREATE TABLE dept_location (
   dno       INT             NOT NULL,
   dlocation      CHAR(10)         NOT NULL,
   FOREIGN KEY (dno)  REFERENCES department (dnum)    ON DELETE CASCADE,
      PRIMARY KEY (dno,dlocation)
); 

CREATE TABLE project (
   pnum     INT             NOT NULL,
    pname     CHAR(15)         NOT NULL,
    dno   INT          NOT NULL,
    plocation    CHAR(10) default 'Man',
    FOREIGN KEY (dno)  REFERENCES department   (dnum)  ON DELETE CASCADE,
    PRIMARY KEY (pnum)
);

CREATE TABLE works_on (
    eNI      INT             NOT NULL,
    pno       INT     NOT NULL,
   hours numeric (4,2),
    FOREIGN KEY (eNI) REFERENCES employee (NI) ON DELETE CASCADE,
    FOREIGN KEY (pno) REFERENCES project (pnum) ON DELETE CASCADE,
    PRIMARY KEY (eNI,pno)
); 

CREATE TABLE dependent (
    eNI     INT             NOT NULL,
   depen_name     char(15)             NOT NULL,
     gender      ENUM ('M','F')  NOT NULL,
    bdate     DATE            NOT NULL,
    relationship char(15),
    FOREIGN KEY (eNI) REFERENCES employee (NI) ON DELETE CASCADE,
    PRIMARY KEY (eNI,depen_name),
    UNIQUE  KEY (depen_name)
); 
ALTER TABLE employee
ADD FOREIGN KEY (superNI) REFERENCES employee (NI) ON DELETE CASCADE,
ADD FOREIGN KEY (dno) REFERENCES department (dnum) ON DELETE CASCADE;



