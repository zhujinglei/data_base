# Introduction to Databases

## Section 1: Database basics
### Lecture 1: Data and DBMS
 * Overview
 * Databases you use in the real world: maps, accounts, ecommerce, netflix, etc
 * What is a database and why?
     * data abstraction
     * multiple views of data, sharing data, meta-data about the data
     * removing needless redundancy 
     * controlling access
     * focusing on data: persistence, organization, access, control, safety
 * file systems
 * databases vs file systems & flat file DBs (redundant data that needs updates, joins are either there or not easy)
 * Spread sheets - still a 'flat file' style, Google sheets: multiple edits, one view
 * Create, Read, Update, Delete - CRUD
 * role of DBMS
 * DB design, implementation, programming
 * DB types: network, hierarchical, relational, object, object relational, …
 * DBMS
     * Early (this lecture)
     * Current state
     * Future - it was all OODB
 * Practical: writing your own in Python
     * Build a simple CRUD system with permanent storage and query syntax
     * What data structures can you use
     * What do you have to manage
     * Installing a basic DBMS

### Lecture : What makes up a DBMS
 * Storage manager
     * Storage considerations:
         * It used to be: Memory, Disk, Offline/Slow storage (tape, etc)
         * Now it's: Memory, SSD, and Disk is slow
 * Query manager
 * Transaction manager
 * Data language parts: DDL, SDL (storage), VDL (view), DML (manipulation)
 * Practical: basic SQL and some NoSQL
     * Working with SQLite, PostgreSQL, MS SQL Server
     * Basic DML and case sensitivity
     * Basic SQL work: Select, From, Where, projection and column id, distinct, simple aggregates: count, max, min, avg, sum
     * Some examples from our code
     * Trying NoSQL key value: Redis
 
### Lecture : Relations and Entities - Relational Data Models
 * Relational Algebra and Calculus
 * entities vs relationships & entity relationship diagrams and limitations
 * Relational data model
 * Modeling with Entity Relationship Model
 * Enhanced ER Model
 * RDB design by ER/EER
 * Calling from applications: simple DB retrieval, connections, sessions
 * SQL data types, format, concatenation, null, is, not, exists, in, all, any, <>/not in
 * Common alternative SQL DBMS: MySQL, MariaDB, CockroachDB
 * Practical: more SQL
     * Designing tables and DBs
     * Using SQL data types and commands learned today
     * Looking at tables from our apps
     * Calling from applications
     * Consider refactoring BlackGraphs from pythonic queries to using SQLite
 
### Lecture : Queries processing
* 3-Schema Architecture: conceptual, logical/external, physical models and hardware layout
* Storage, File structures, hashing
* Indexing structures for files
* Query processing 
    * isolation levels, consistency
    * execution plan
* Physical tuning and design
* Impedance mismatch
* Object and Object Relational DB
* SQL: updates, deletes, insertions, autonumbering
* Practical: changing data and more NoSQL
    * Updates, deletes, etc
    * Query planner and execution 
    * Isolation levels
    * More NoSQL document/search: ElasticSearch
        * What's it good for and why we use it
        * Simple schema-less design
        * Our schema and what kind of queries (consider the anomaly sys using ES as example as well as search)

### Lecture : Defining the relationships: Schema, Keys, and more
 * Normalization - 3rd, 4th, BCNF, ETNF, ...
 * Relational DB design approaches
 * DB design and UML
 * schemas
 * schemas, catalogs, clusters, DBMS - going up the hierarchy
 * schemas, character sets, collations, translations - smaller refinements
 * referential integrity
 * keys, joins - left, right, inner, outer, 1=1
 * composite keys, foreign keys
 * XML
 * Set operations - union, intersection, difference
 * Sets and Bags - it's really a bag
 * Debugging queries
 * Practical: table changes and constraints
     * Create, drop, alter tables, default values, create DB
     * Constraints: unique, primary key, length, not null, check, on update, on delete, assertions and tuples, alter: add, drop
     * Debugging queries
     * Looking at our tables - too many or too few constraints
     
### Lecture : Speeding up 
 * Normalisation and denormalization
 * Index/indices/indexes
 * compiled vs dynamic queries
 * read isolation levels again
 * Concurrency control
 * Distributed DBs and the CAP theorem
 * ORM: object relational mapping
 * Debugging queries
 * DB & querying pitfalls
 * Practical: Fun with joins 
     * complex queries, aggregates, order by, group by, having
     * Cartesian product, natural joins, theta joins, clarifying attributes/columns, null and outer joins
     * Union, intersection, except/difference
     * Schema modification
     * More examples from our systems and how would you write these complex queries

### Lecture : Transactions
 * Transactions and locks
 * ACID
 * providing locks or checks outside of transactions: long running changes without explicit transactions - 'checked out/closed/open'
 * transaction isolation levels
 * Transaction processing, commits, rollbacks
 * Practical: Transactions
     * Running transactions, committing, rolling back, locking everyone else out
     * NoSQL columnar: Cassandra, InfluxDB

### Lecture : Making more complex SQL
 * Subqueries
 * DB security, grant, allow, admin, users, revoke
 * View: create, select, alter, drop, insert, delete
 * Variables: simple, table
 * With statements
 * Comparison of strings, like/%, convert
 * More on dates
 * while loop
 * Stored procedures
 * Cursors and when they're ok (for us, don't), fetch
 * DB recovery, dumps
 * Practical: getting more complex
     * Using the SQL from today to create more complex SQL
     * NoSQL document: Couchbase DB (Mongo)

### Lecture : Automating
 * Automating actions in the DB: triggers
 * Repeatable actions on the DB: versioning (liquibase, flyaway, etc), cron jobs, scheduled jobs
 * Practical: versioning and triggers

### Lecture : Data Warehousing
 * Data warehousing
 * Type I II III IV VI changes
 * Data cubes
 * Information Retrieval and text processing
 * Data mining
 * DWH and OLAP vs OLTP
 * Practical:
     * Working with different type changes
     * Where we use them outside of DWH

### Lecture: Big Data Intro
 * Data stores
 * Retrieving
 * Querying data over calls (graphql, elephantdb, etc)
 * Practical:
     * Having fun with big data on a small scale
