# Links to recommeded Database reading list


* Download links
    * [Fundamentals of Database Systems (7th Edition)](http://noahc.me/Fundamentals%20of%20Database%20Systems%20%287th%20edition%29.pdf)
    * [A First Course in Database Systems (3rd Edition)](https://www.scribd.com/document/326994007/A-First-Course-in-Database-Systems-3rd-Edition-pdf)
    * [Database Systems:The Complete Book (2nd Edition)](https://people.inf.elte.hu/miiqaai/elektroModulatorDva.pdf)
    * [Database System Concepts (6th Edition)](https://kakeboksen.td.org.uit.no/Database%20System%20Concepts%206th%20edition.pdf)