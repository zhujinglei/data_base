# Links to installing or using databases

#### SQL DBMS:
* Download links
    * https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup?view=sql-server-2017
    * https://www.postgresql.org/download/
    * https://www.sqlite.org/download.html
    * https://www.cockroachlabs.com/
    * https://www.mysql.com/downloads/
    * https://downloads.mariadb.org/

#### SQL UIs:
* All SQL: 
    * https://dbeaver.io/download/
    * using Eclipse and PyCharm/IntelliJ
    * http://www.squirrelsql.org/#installation
* SQL Server: https://docs.microsoft.com/en-us/sql/sql-operations-studio/download
* SQL Server (Win only?): https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017
* PostreSQL: https://www.pgadmin.org/download/
* MySql: https://www.mysql.com/products/workbench/
* DB Browser for SQLite: https://sqlitebrowser.org/


#### Microsoft SQL Server for Python - Create Python apps using SQL Server on RedHat systems:
* https://www.microsoft.com/en-us/sql-server/developer-get-started/python/rhel/

#### NoSQL DBMS:
* Redis: https://redis.io/download
* CouchbaseDB: https://www.couchbase.com/downloads
* Cassandra: http://cassandra.apache.org/download/
* ElasticSearch: https://www.elastic.co/downloads/elasticsearch
